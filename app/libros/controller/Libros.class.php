<?php

/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 28/08/2017
 * Time: 11:30 AM
 */
class Libro
{

    public static function getLibros()
    {
            $libros = DB::query("SELECT * FROM libros");

        $dataLib = []; $html = null;

        while ($valor = $libros->fetch_object()) {

            $valor->precio_unit = '$ '.number_format($valor->precio_unit, 0, ",", ".");
            $dataLib[] = $valor;

            $html .= '<div class="col-md-6 area-book"> 
                      <div class="col-md-5 text-center">
                      <img src="'.$valor->imagen.'"
                      width="190" height="270" alt="JUEGOS-DE-TRONOS-2" id="">
                      </div>
                      <div class="col-md-7 text-left">
                      <h4>'.$valor->nombre.'</h4>
                      <br>';

                  if ($valor->disponibles == 0){
                      $html .= '<p>Cantidad disponible: <label class="alert-danger"> Agotado </label></p>';
                      $html .= '<p>Precio: '.$valor->precio_unit.'</p>
                      <br>
                      <br>
                      </div>
                      </div>';

                  }   else {
                      $html .= '<p>Cantidad disponible: '.$valor->disponibles.'</p>';

                      $html .= '<p>Precio: '.$valor->precio_unit.'</p>
                      <br>
                      <br>
                      <div class="form-group col-md-6">
                      <label for="cantidad-'.$valor->id.'">Cantidad:</label>
                      <select class="form-control" name="cantidad-'.$valor->id.'" id="cantidad-'.$valor->id.'" ';

                      for ($l=0; $l<= $valor->disponibles ; $l++)
                      {
                          $html .= '<option value="'.$l.'">'.$l.'</option>';
                      }

                      $html .= '</select>
                      </div>
                      <div class="col-md-6">
                      <button id="'.$valor->id.'" type="button" onclick="$.setPedido(this.id)" class="btn btn-primary" style="margin-top: 25px">Agregar al carrito</button>
                      </div>
                      </div>
                      </div>';
                  }


        }



        return array(
            'data' => $dataLib,
            'html' => $html
        );
    }

    public static function sendPedido($libroid,$cantidad)
    {

        $validape = DB::query("SELECT COUNT(id) as conta FROM pedidos WHERE estado = 'proceso' ")->fetch_object()->conta;

        if($validape >= 0){

            $pedidos = DB::query("INSERT INTO pedidos (estado,total)
        VALUES ('proceso', 0 )");

            $idpedidos =  DB::getMySQLiObject()-> affected_rows;
            $idped = DB::getMySQLiObject()->insert_id;

        }
        else{

            $idpedidos = 'En proceso';
            $idped = DB::query("SELECT max(id) FROM pedidos WHERE estado = 'proceso' ")->fetch_object()->id;


        }


        $precio = DB::query("SELECT precio_unit FROM libros WHERE id =".(int)$libroid)->fetch_object()->precio_unit;

        $lpedidos = DB::query("INSERT INTO libros_pedidos (pedido_id,libro_id,unidades,total)
        VALUES (
        ".(int)$idped .",
        ".(int)$libroid.",
        ".(int)$cantidad.",
        ".((int)$cantidad * (int)$precio)."
        )");
        $idlpedidos = DB::getMySQLiObject()-> affected_rows;




        return array('pedidos' => $idpedidos, 'lpedidos' => $idlpedidos, 'precio' => $precio);
    }


    public static function getPedidos()
    {
        $pedidos = DB::query("SELECT SQL_CALC_FOUND_ROWS
        lp.id idpedido,
        lb.nombre nombre,
        lp.unidades cantidad,
        lb.precio_unit precio
        FROM libros lb 
        JOIN libros_pedidos lp ON lb.id = lp.libro_id
        JOIN pedidos pe ON lp.pedido_id = pe.id
        WHERE pe.estado = 'proceso' ");

        $dataLib = [];
        $totFinal = 0;

        while ($valor = $pedidos->fetch_object()) {

            $tot = (int)$valor->precio * (int)$valor->cantidad;
            $valor->precio = '$ '.number_format($valor->precio, 0, ",", ".");
            $valor->total = '$ '.number_format($tot, 0, ",", ".");
            $valor->edit = '<button class="btn btn-danger" onclick="$.deletePedido(this.id)"><i class="fa fa-trash" aria-hidden="true"></i></span> </button>';
            $dataLib[] = $valor;

            $totFinal = $totFinal + $tot;
        }

        $condata = 'SELECT FOUND_ROWS() as tot';
        $totaldata = DB::query($condata)->fetch_object() -> tot;

        return array('draw' => 0 ,'recordsTotal' => $totaldata , 'recordsFiltered' => $totaldata ,
            'data' => $dataLib, 'totalReal' => '$ '.number_format($totFinal, 0, ",", "."));
    }
}