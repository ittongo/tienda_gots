<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require '../../database/DB.instance.class.php';
require '../controller/Libros.class.php';

try{

    $response = array();

    // Handling the supported actions:

    switch($_GET['action']){

        case 'getLibros':

            $response = Libro::getLibros();
            break;

        case 'sendPedido':

            $response = Libro::sendPedido($_POST['id'],$_POST['cantidad']);
            break;

        case 'getPedidos':

            $response = Libro::getPedidos();
            break;

        default:
            throw new Exception('Wrong action');
    }

    echo json_encode($response);
}
catch(Exception $e){
    die(json_encode(array('error' => $e->getMessage())));
}

?>