<!-- Desarrollado por Jorge Gil -->

<!DOCTYPE html>
<html lang="es">

<?php include_once ('./head.php') ?>

<body>
<div class="col-md-12">

    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation"><a href="./index.php"><i class="fa fa-book" aria-hidden="true"></i> Lista de libros</a></li>
                <li role="presentation" class="active"><a href="./carrito_compras.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Carrito de compras</a></li>
                <li role="presentation"><a href="./pedidos.php"><i class="fa fa-server" aria-hidden="true"></i> Pedidos</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">GOT Books - Carrito de Compras</h3>
    </div>

    <table id="gridCarrito" class="table table-striped table-bordered table-hover table-full-width"
           style="border: none !important ">
        <thead>
        <tr>
            <th>Id de Pedido</th>
            <th>Nombre de Libro</th>
            <th>Cantidad</th>
            <th>Precio Unitario</th>
            <th>Precio Total</th>
            <th></th>
        </tr>
        </thead>
    </table>
      <div class="col-md-12">
    <h4 class="alert-danger">Total del pedido: </h4><span id="tr"></span>
    <br>
      </div>

    <div class="col-md-12" style="margin-top: 10px">
        <div class="col-md-6 text-center">
            <button class="btn btn-danger"> Cancelar la compra </button>
        </div>
        <div class="col-md-6 text-center">
            <button class="btn btn-primary"> Confirmar la compra </button>
        </div>

    </div>

    <?php include_once ('./footer.php')?>
</div>

<?php include_once ('./scriptsjs.php') ?>


<script src="./js/carrito.js"></script>
<script>
    /* Ejecuto la función principal llamada Index para cuando el documento haya terminado de cargar el DOM */
    jQuery(document).ready(function () {
        index.init();
    });
</script>
</body>

</html>