<head>

    <!-- Agrego los atributos meta para dar una mejor identidad a la pagina en los motores de busqueda-->
    <meta charset="UTF-8">
    <title>GOT Books - Tienda Online</title>
    <meta name="author" content="Jorge Eduardo Gil">
    <meta name="description" content="Tienda de Game of Thrones">
    <meta name="copyright" content="Derechos de Jorge Gil">
    <meta name="keywords" content="got,game of thrones">

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/3.3/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">

    <!-- Traigo el estlo general de plantilla central basado en los estilos externos llamados de bootstrap-->
    <link href="./css/plantilla-inicio.css" rel="stylesheet">

    <link href="./css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="./js/plugins/DataTables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" href="./js/plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet" href="//fontawesome.io/assets/font-awesome/css/font-awesome.css">


</head>