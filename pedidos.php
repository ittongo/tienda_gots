<!-- Desarrollado por Jorge Gil -->

<!DOCTYPE html>
<html lang="es">

<?php include_once ('./head.php') ?>

<body>
<div class="col-md-12">

    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation"><a href="./index.php"><i class="fa fa-book" aria-hidden="true"></i> Lista de libros</a></li>
                <li role="presentation"><a href="./carrito_compras.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Carrito de compras</a></li>
                <li role="presentation" class="active"><a href="./pedidos.php"><i class="fa fa-server" aria-hidden="true"></i> Pedidos</a></li>
            </ul>
        </nav>
        <h4 class="text-muted">GOT Books - Pedidos</h4>
    </div>

    <h2 class="alert-warning">Aun no tienes pedidos disponibles !</h2>

    <?php include_once ('./footer.php')?>
</div>

<?php include_once ('./scriptsjs.php') ?>


<script src="./js/pedidos.js"></script>
<script>
    /* Ejecuto la función principal llamada Index para cuando el documento haya terminado de cargar el DOM */
    jQuery(document).ready(function () {
        index.init();
    });
</script>
</body>

</html>