var index = function() {

    // Controladores GET & POST metodos:

    $.tzPOST = function (module, action, data, callback) {
        $.post('./app/'+module+'/route/ajax.php?action=' + action, data, callback, 'json');
    };

    $.tzGET = function (module, action, data, callback) {
        $.get('./app/'+module+'/route/ajax.php?action=' + action, data, callback, 'json');
    };

    $.deletePedido = function (id) {

        var val;
        var r = confirm("Estas seguro de eliminar este libro del pedido ?");
        if (r == true) {
            val = true;
        } else {
            val = false;
        }
    };

    var initCarrito = function () {
        

            
            var i = 0;
            var table =  $('#gridCarrito').DataTable( {
                "processing": true,
                "serverSide": true,
                "pagingType": "full_numbers",
                "deferRender": true,
                "ajax": {
                    url: "./app/libros/route/ajax.php?action=getPedidos",
                    type: "POST",
                    data: { },
                    dataType: "json",
                    dataSrc: function ( r ) {

                        var tr = r.totalReal;
                        $('span#tr').text(tr);

                        return r.data;

                    }
                },
                "columns": [
                    {data: "idpedido", name:"lp.id", visible:false, searchable: true},
                    {data: "nombre", name:"lb.nombre",  width: 290 },
                    {data: "cantidad" ,name: "lp.unidades" , width: 15},
                    {data: "precio", name: "lb.precio_unit"},
                    {data: "total", name:"total"},
                    {data: "edit", searchable: false, orderable: false,width: 10}
                ],
                "aoColumnDefs" : [{
                    "aTargets" : [0]
                }],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_  Pedidos",
                    "sEmptyTable": "No se encontraron resultados para esta busqueda.",
                    "sInfoEmpty": "Mostrando 0 a 0 Pedidos de 0 encontrados",
                    "sInfoFiltered": "(Filtrado de _MAX_ Pedidos)",
                    "sSearch": "",
                    "sSearchPlaceholder": "Busca por nombre, cantidad o precio",
                    "sZeroRecords": "No se encontraron Pedidos relacionados a la busqueda",
                    "sInfo": "Mostrando _START_ a _END_ Pedidos de _TOTAL_ encontrados",
                    "sProcessing": "Cargando pedidos ..",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sPrevious": "Anterior",
                        "info": "Pagina _PAGE_ de _PAGES_",
                        "sNext": "Siguiente",
                        "sLast": "Ultimo"
                    }
                },
                "aaSorting" : [[0, 'desc']],
                "aLengthMenu": [[5, 10, 15, 20, 50, 100, 300, 500, 1000, 5000 ], [5, 10, 15, 20, 50, 100, 300, 500, 1000, 5000 ] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                buttons: [
                    'colvis',
                    'excel',
                    'print'
                ]
            } );

            $('#gridCarrito_wrapper .dataTables_filter input').addClass("input-sm").attr({"style": "width: 400px"});


            $('#gridCarrito_wrapper .dataTables_length select').addClass("m-wrap small");
           


            $('#gridCarrito tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );

            $('#gridCarrito').attr({"style": "max-width: 965px"});
            $('#gridCarrito_wrapper div.select2-container').attr({"style": "width: 80px"});


      

    };


    return {
        init: function() {
            initCarrito()
        }
    };
}();
