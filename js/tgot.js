var index = function() {

    // Controladores GET & POST metodos:

    $.tzPOST = function (module, action, data, callback) {
        $.post('./app/'+module+'/route/ajax.php?action=' + action, data, callback, 'json');
    };

    $.tzGET = function (module, action, data, callback) {
        $.get('./app/'+module+'/route/ajax.php?action=' + action, data, callback, 'json');
    };


    $.setPedido = function (id) {
        
        var cantidad = $('#cantidad-'+id).val();

        $.tzPOST('libros','sendPedido', {id: id, cantidad: cantidad}, function (r) {
               if (r.lpedidos == 1 && r.pedidos == 1){
                    alert('Se ha añadido el libro al carrito de compras !')
               }
        });
    };


    var initLibros = function () {


        
        $.tzPOST('libros','getLibros', {}, function (r) {

            if (r.html != null)
            {

                var containerLibro = $('#contLib');
                  containerLibro.append(r.html);


            }

        });

    };


    return {
        init: function() {
            initLibros()
        }
    };
}();
